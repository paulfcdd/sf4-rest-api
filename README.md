How to setup this project.
1) clone repo on your computer
2) run command "composer install" and follow the instructions
3) configure database connection in `.env` file in the root of the project
4) If you would like to start tests, configire DB connection also in `phpunit.xml.dist` file in the root of the project
5) run command 'bin/console doctrine:schema:update --force' to fill your database with all necessary tables