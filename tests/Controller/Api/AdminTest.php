<?php

namespace App\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminTest extends WebTestCase
{
    public function testGetProductList()
    {
        $client = static::createClient([
            'environment' => 'test'
        ]);
        $client->request('GET', '/api/admin/get-products');


        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testAddProduct()
    {
        $data = [
            'name' => 'Some good',
            'bar_code' => 54446,
            'cost' => 255,
            'vat_class' => 6
        ];

        $client = static::createClient();
        $client->request('PUT', '/api/admin/add-product', [], [], [], json_encode($data));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}