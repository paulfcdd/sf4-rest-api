<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="receipts")
 */
class Receipt extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column()
     */
    private $name;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isFinished = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReceiptDetail", mappedBy="receipt")
     */
    private $receiptDetails;

    public function __construct()
    {
        $this->receiptDetails = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return Receipt
     */
    public function setName(?string $name): Receipt
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isFinished(): ?bool
    {
        return $this->isFinished;
    }

    /**
     * @param bool|null $isFinished
     * @return Receipt
     */
    public function setIsFinished(?bool $isFinished): Receipt
    {
        $this->isFinished = $isFinished;
        return $this;
    }

    /**
     * @param ReceiptDetail $receiptDetail
     * @return $this
     */
    public function addProductReceipt(ReceiptDetail $receiptDetail)
    {
        if (!$this->receiptDetails->contains($receiptDetail)) {
            $this->receiptDetails->add($receiptDetail);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProductReceipts()
    {
        return $this->receiptDetails;
    }
}