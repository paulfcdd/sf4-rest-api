<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 *
 * @ORM\Table(name="products")
 */
class Product extends AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column()
     */
    private $barCode;

    /**
     * @var string|null
     * @ORM\Column()
     */
    private $name;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2)
     */
    private $cost;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $vatClass;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReceiptDetail", mappedBy="product")
     */
    private $receiptDetails;

    public function __construct()
    {
        $this->receiptDetails = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getBarCode(): ?string
    {
        return $this->barCode;
    }

    /**
     * @param null|string $barCode
     * @return Product
     */
    public function setBarCode(?string $barCode): Product
    {
        $this->barCode = $barCode;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return Product
     */
    public function setName(?string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCost(): ?float
    {
        return $this->cost;
    }

    /**
     * @param float|null $cost
     * @return Product
     */
    public function setCost(?float $cost): Product
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getVatClass(): ?int
    {
        return $this->vatClass;
    }

    /**
     * @param int|null $vatClass
     * @return Product
     */
    public function setVatClass(?int $vatClass): Product
    {
        $this->vatClass = $vatClass;
        return $this;
    }

    /**
     * @param ReceiptDetail $receiptDetail
     * @return $this
     */
    public function addProductReceipt(ReceiptDetail $receiptDetail)
    {
        if (!$this->receiptDetails->contains($receiptDetail)) {
            $this->receiptDetails->add($receiptDetail);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProductReceipts()
    {
        return $this->receiptDetails;
    }
}