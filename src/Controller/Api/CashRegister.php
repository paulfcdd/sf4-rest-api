<?php

namespace App\Controller\Api;

use App\Entity\Product;
use App\Entity\ReceiptDetail;
use App\Entity\Receipt;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CashRegister extends AbstractController
{
   public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer, RequestStack $requestStack)
   {
       parent::__construct($entityManager, $serializer, $requestStack);
   }

    /**
     * @Route(
     *     "/api/cash-register/get-product-by-barcode",
     *     name="app.api.cash_register.get_product_by_barcode",
     *     methods={"GET"}
     *     )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getProductByBarcode(Request $request)
    {
        try {
            /** @var \stdClass $requestContent */
            $requestContent = $this->getRequestContent($request);
            $product = $this->entityManager->getRepository(Product::class)->getProductByBarcode($requestContent->barcode);

            return $this->jsonSuccessResponse(
                $this->serializeDataToFormat($product, 'json')
            );

        } catch (\Exception $exception) {
            return $this->jsonErrorResponse($exception->getMessage());
        }
    }

    /**
     * @Route(
     *     "/api/cash-register/new-receipt",
     *     name="app.api.cash_register.new_receipt",
     *     methods={"PUT"}
     *     )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createReceipt(Request $request)
    {
        $requestContent = $this->getRequestContent($request);
        $receipt = new Receipt();

        $receipt->setName($requestContent->name);

        try {
            $this->entityManager->persist($receipt);
            $this->entityManager->flush();
            return $this->jsonSuccessResponse('Receipt created');
        } catch (\Exception $exception) {
            return $this->jsonErrorResponse($exception->getMessage());
        }

    }

    /**
     * @Route(
     *     "/api/cash-register/add-product-to-receipt",
     *     name="app.api.cash_register.add_product_to_receipt",
     *     methods={"POST", "PUT"}
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function addProductByBarcodeToReceipt(Request $request)
    {
        $requestContent = $this->getRequestContent($request);
        /** @var Receipt $receipt */
        $receipt = $this->entityManager->getRepository(Receipt::class)->findOneById($requestContent->receipt_id);
        /** @var Product $product */
        $product = $this->entityManager->getRepository(Product::class)->getProductByBarcode($requestContent->product_barcode);
        /** @var ReceiptDetail $productReceipt */
        $productReceipt = new ReceiptDetail();

        if ($receipt->isFinished()) {
            return $this->jsonErrorResponse('You can not add products to finished receipt');
        }

        try {
            $productReceipt
                ->setReceipt($receipt)
                ->setProduct($product)
                ->setQuantity($requestContent->quantity);

            $this->entityManager->persist($productReceipt);
            $this->entityManager->flush();

            return $this->jsonSuccessResponse('Product was added to receipt');
        } catch (\Exception $exception) {
            return $this->jsonErrorResponse('Can not add product due to: ' . $exception->getMessage());
        }
    }

    /**
     * @Route(
     *     "/api/cash-register/change-last-product-amount",
     *     name="app.api.cash_register.change_last_product_amount",
     *     methods={"POST"}
     * )
     *
     * @return JsonResponse
     *
     * @param Request $request
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function changeAmountOfLastProductInReceipt(Request $request)
    {
        $requestContent = $this->getRequestContent($request);
        /** @var Receipt $receipt */
        $receipt = $this->entityManager->getRepository(Receipt::class)->findOneById($requestContent->receipt_id);

        if ($receipt->isFinished()) {
            return $this->jsonErrorResponse('Receipt was finished');
        }

        /** @var ReceiptDetail $lastProduct */
        $lastProduct = $this->entityManager->getRepository(ReceiptDetail::class)->getLastProductInReceipt($requestContent->receipt_id);

        try {
            $lastProduct->setQuantity($requestContent->product_quantity);
            $this->entityManager->flush();

            return $this->jsonSuccessResponse('Quantity was changed');
        } catch (\Exception $exception) {
            return $this->jsonErrorResponse($exception->getMessage());
        }
    }

    /**
     * @Route(
     *     "/api/cash-register/finish-receipt",
     *     name="app.api.cash_register.finish_receipt",
     *     methods={"POST"}
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function finishReceipt(Request $request)
    {
        $requestContent = $this->getRequestContent($request);
        /** @var Receipt $receipt */
        $receipt = $this->entityManager->getRepository(Receipt::class)->findOneById($requestContent->receipt_id);

        try {
            $receipt->setIsFinished(true);
            $this->entityManager->flush();

            return $this->jsonSuccessResponse('Receipt was finished');
        } catch (\Exception $exception) {
            return $this->jsonErrorResponse($exception->getMessage());
        }
    }

    /**
     * @Route(
     *     "/api/cash-register/get-receipt",
     *     name="app.api.cash_register.get_receipt",
     *     methods={"GET"}
     * )
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getReceiptData(Request $request)
    {
        try {
            $requestContent = $this->getRequestContent($request);
            $receipt = $this->entityManager->getRepository(Receipt::class)->findOneById($requestContent->receipt_id);
            $receiptComponents = $this->entityManager->getRepository(ReceiptDetail::class)->findBy(['receipt' => $receipt]);
            $response = [];
            $i = 1;

            /** @var ReceiptDetail $receiptComponent */
            foreach ($receiptComponents as $receiptComponent) {
                $quantity = $receiptComponent->getQuantity();
                $unitCost = $receiptComponent->getProduct()->getCost();
                $totalCost = $quantity * $unitCost;
                $productVat = $receiptComponent->getProduct()->getVatClass();

                $response['products']['receipt_item_' . $i]['product_name'] = $receiptComponent->getProduct()->getName();
                $response['products']['receipt_item_' . $i]['unit_cost'] = $unitCost;
                $response['products']['receipt_item_' . $i]['quantity'] = $quantity;
                $response['products']['receipt_item_' . $i]['total_cost'] = $quantity * $unitCost;
                $response['products']['receipt_item_' . $i]['total_vat'] = round(($productVat / 100) * $totalCost, 2);

                $i++;
            }

            return $this->jsonSuccessResponse($response);
        } catch (\Exception $exception) {
            return $this->jsonErrorResponse('Can not get receipt details.');
        }
    }
}