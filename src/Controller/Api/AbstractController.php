<?php

namespace App\Controller\Api;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class AbstractController extends Controller implements EventSubscriberInterface
{
    /** @var int  */
    public const SUCCESS_CODE = 200;

    /** @var int  */
    public const ERROR_CODE = 500;

    /** @var int  */
    public const AUTH_FAILED = 403;

    /** @var EntityManagerInterface  */
    public $entityManager;

    /** @var SerializerInterface  */
    public $serializer;

    public $requestStack;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
        $this->initSession();
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if ($event->getRequest()->headers->get('user-api-token') !== $this->initSession()->get('user_api_token')) {
            throw new AccessDeniedHttpException('This action needs a valid token!');
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
        );
    }

    public function initSession()
    {
        $session = new Session();

        if (!$session->get('user_api_token')) {
            $session->set('user_api_token', rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '='));
        }

        return $session;
    }

    /**
     * @param mixed $data
     * @param int $status
     *
     * @return JsonResponse
     */
    public final function jsonSuccessResponse($data, int $status = self::SUCCESS_CODE)
    {
        return JsonResponse::create($data, $status);
    }

    /**
     * @param mixed $data
     * @param int $status
     *
     * @return JsonResponse
     */
    public function jsonErrorResponse($data, int $status = self::ERROR_CODE)
    {
        return JsonResponse::create($data, $status);
    }

    /**
     * @param Request $request
     *
     * @return \stdClass
     */
    public final function getRequestContent(Request $request)
    {
        return json_decode($request->getContent());
    }

    /**
     * @param mixed $data
     *
     * @param string $format
     *
     * @return mixed
     */
    public final function serializeDataToFormat($data, string $format)
    {
        return $this->serializer->serialize($data, $format);
    }
}