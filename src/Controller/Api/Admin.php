<?php

namespace App\Controller\Api;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class Admin extends AbstractController
{
    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer, RequestStack $requestStack)
    {
        parent::__construct($entityManager, $serializer, $requestStack);
    }

    /**
     * @Route("/api/admin/add-product", name="app.api.add_product", methods={"PUT"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addProduct(Request $request)
    {
        $product = new Product();
        $productData = $this->getRequestContent($request);

        $product
            ->setName($productData->name)
            ->setBarCode($productData->bar_code)
            ->setCost($productData->cost)
            ->setVatClass($productData->vat_class);

        try {
            $this->entityManager->persist($product);
            $this->entityManager->flush();
            return $this->jsonSuccessResponse('Product saved');
        } catch (\Exception $exception) {
            return $this->jsonErrorResponse($exception->getMessage());
        }
    }

    /**
     * @Route("/api/admin/get-products", name="app.api.get_products", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getProductList()
    {
        try {
            $productsList = $this->entityManager->getRepository(Product::class)->getProductsList();
            $serializedList = $this->serializeDataToFormat($productsList, 'json');

            return $this->jsonSuccessResponse($serializedList);
        } catch (\Exception $exception) {
            return $this->jsonErrorResponse(
                'Error during getting list of products '. $exception->getMessage()
            );
        }
    }
}