<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public const TABLE_ALIAS = 'p';

    /**
     * @param int $barcode
     *
     * @return Product
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getProductByBarcode(int $barcode)
    {
        return $this->createQueryBuilder(self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.barCode = :barcode')
            ->setParameter('barcode', $barcode)
            ->getQuery()
            ->getSingleResult();
    }

    public function getProductsList()
    {
        return $this->createQueryBuilder(self::TABLE_ALIAS)
            ->select(
                self::TABLE_ALIAS . '.id',
                self::TABLE_ALIAS . '.name',
                self::TABLE_ALIAS . '.cost',
                self::TABLE_ALIAS . '.barCode',
                self::TABLE_ALIAS . '.vatClass'
            )
            ->getQuery()
            ->getResult();
    }
}