<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ProductReceiptRepository extends EntityRepository
{
    public const TABLE_ALIAS = 'pr';

    /**
     * @param int $receipt
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastProductInReceipt(int $receipt)
    {
        return $this->createQueryBuilder(self::TABLE_ALIAS)
            ->where(self::TABLE_ALIAS . '.receipt = :receipt')
            ->orderBy(self::TABLE_ALIAS . '.product', 'ASC')
            ->setMaxResults(1)
            ->setParameter('receipt', $receipt)
            ->getQuery()
            ->getSingleResult();
    }
}